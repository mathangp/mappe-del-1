package mappe.del1.hospital;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The Department class is a mutable class.
 * @author Mathangi Pushparajah
 */
public class Department {

    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * The list of employees and patients are set to an empty list in the constructor.
     * @param departmentName takes in an argument and sets the name of the department
     *                       using a method called "setDepartmentName"
     */
    public Department(String departmentName) {
        setDepartmentName(departmentName);
        employees = new ArrayList<>();
        patients = new ArrayList<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Method checks for valid input of the department name. Throws and exception
     * if the name is not valid.
     * @param departmentName takes in an argument and sets the name of the department
     *                       if argument is valid.
     */
    public void setDepartmentName(String departmentName) {
        if (departmentName.isBlank()){
            throw new IllegalArgumentException("Please enter a valid name for the department");
        }
        this.departmentName = departmentName;
    }

    /**
     * The method adds an Employee object only if it does not already exist.
     * @param employee an argument of type Employee
     */
    public void addEmployee(Employee employee) {
        Employee employeeCopy = new Employee(employee.getFirstName(), employee.getLastName(),
                employee.getSocialSecurityNumber());

        if (employees.isEmpty()){
            employees.add(employeeCopy);
        } else {
            for (Employee object: employees){
                if (!object.equals(employeeCopy)){
                    employees.add(employeeCopy);
                }
            }
        }
    }

    /**
     * The method adds a Patient object only if it does not already exist.
     * @param patient an argument of type Patient
     */
    public void addPatient(Patient patient){
        Patient patientCopy = new Patient(patient.getFirstName(), patient.getLastName(),
                patient.getSocialSecurityNumber());

        if (patients.isEmpty()){
            patients.add(patientCopy);
        }else{
            for (Patient object : getPatients()){
                if (!object.equals(patientCopy)){
                    patients.add(patientCopy);
                }
            }
        }
    }

    /**
     * This method makes a list which consists of only employees.
     * @return a list of all employees.
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * This method makes a list which consists of only patients.
     * @return a list of all patients.
     */
    public List<Patient> getPatients(){
        return patients;
    }

    /**
     * The method check of the given object is of type Employee or Patient
     * before trying the remove the object from the list (either employees or patients)
     * @param person takes in an argument of type Person
     * @throws RemoveException an exception is thrown if the person object was not removed successfully.
     */
    public void remove (Person person) throws RemoveException {
        if (person instanceof Employee){
            boolean success1 = employees.remove(person);
            if (success1 == false){
                throw new RemoveException("The employee- " + person.getFullName()+
                        " is not registered in the hospital");
            }
        }

        if (person instanceof Patient){
            boolean success2 = patients.remove(person);
            if (success2 == false){
                throw new RemoveException("The patient- " + person.getFullName()+
                        " is not registered in the hospital");
            }
        }
    }

    /**
     * The method checks if the references of the objects are the same and
     * and are considered as equal if it is the case.
     *
     * It the object is null og the class type of the two objects do not match,
     * then the objects are not equal.
     *
     * Objects are considered equal if the name of their department is equal.
     * @param obj takes in an argument of type object
     * @return true if the two objects that are being compared are equal,
     *          false if the two objects that are being compared are not equal.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Department that = (Department) obj;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {
        String textEmployees = "";
        String textPatients = "";

        for (Employee employee: employees){
            textEmployees += employee.toString() + "\n";
        }
        for (Patient patient: patients){
            textPatients += patient.toString() + "\n";
        }

        return "Department name = " + departmentName + '\n' +
                "List over all employees: \n" + textEmployees +
                "\nList over all patients: \n" + textPatients;
    }
}
