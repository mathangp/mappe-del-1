package mappe.del1.hospital;

import java.util.ArrayList;
import java.util.List;

/**
 * The Hospital class is a mutable class.
 * @author Mathangi Pushparajah
 */
public class Hospital {

    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * The constructor defines the the department list as an empty list.
     * @param hospitalName an argument which is taken in by the constructor
     *                     which sets the name of the hospital.
     *                     If the name is not valid, then it throws an exception.
     */
    public Hospital(String hospitalName){
        if (hospitalName.isBlank()){
            throw new IllegalArgumentException("Please enter a valid name for the hospital");
        }
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * This method makes a new copy of the list departments so that changes done to the copy
     * would not affect the original list.
     * @return a list with a copy of all departments
     */
    public List<Department> getDepartments() {
        return departments;
    }

    /**
     * The method checks if the given parameter exists in the list, if it does not,
     * it then add the given parameter to the department list.
     * @param department takes in a argument of type Department
     */
    public void addDepartment(Department department){
        Department departmentCopy = new Department(department.getDepartmentName());
        for (Department object: departments){
            if (!object.equals(departmentCopy)){
                departments.add(departmentCopy);
            }
        }
    }

    @Override
    public String toString() {
        String textDepartments = "";
        for (Department department: departments) {
            textDepartments += department.getDepartmentName() + "\n";
        }
        return "Hospital name: " + hospitalName +
                "\nAll departments are listed below: \n" + textDepartments;
    }
}
