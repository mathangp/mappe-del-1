package mappe.del1.hospital;

public class HospitalClient {

    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("Hospit");
        System.out.println("Test 1\n");
        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println(hospital);

        System.out.println("Test 2\n");
        System.out.println(hospital.getDepartments().get(0)); // Before remove method
        hospital.getDepartments().get(0).remove(new Employee("Odd Even", "Primtallet", ""));
        System.out.println(hospital.getDepartments().get(0)); //After remove method

        System.out.println("Test 3\n");
        try{
            hospital.getDepartments().get(0).remove(new Patient("O", "A", ""));
        }catch (Exception error){
            System.out.println(error.getMessage());
        }
    }
}
