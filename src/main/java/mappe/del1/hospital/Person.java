package mappe.del1.hospital;

import java.util.Objects;

public abstract class Person {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if (firstName.isBlank() || lastName.isBlank()){
            throw new IllegalArgumentException("Please enter valid information");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return lastName + ", " + firstName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setFirstName(String firstName) {
        if (firstName.isBlank()){
            throw new IllegalArgumentException("Please enter a valid first name");
        }
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        if (lastName.isBlank()){
            throw new IllegalArgumentException("Please enter a valid last name");
        }
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if (socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("Please enter a valid social security number");
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Name = " + firstName + ' ' + lastName + ", " +
                "Social Security Number = " + socialSecurityNumber;
    }
}
