package mappe.del1.hospital;

public class RemoveException extends Exception {

    private final static long serialVersionUID = 1L;

    public RemoveException(String message) {
        super("Object cannot be found: " + message);
    }
}
