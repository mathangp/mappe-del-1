package mappe.del1.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentTest {

    @Test
    public void positiveRemoveTest() throws RemoveException{
        Department department = new Department("Something");
        Patient patient = new Patient("Mathangi", "Pushparajah", "12345");
        Employee employee = new Employee("Lucid", "Dream", "3456");
        department.addEmployee(employee);
        department.addPatient(patient);

        department.remove(employee);
        assertFalse(department.getEmployees().contains(employee));

    }

    @Test
    public void negativeRemoveTest() throws RemoveException{
        Department department = new Department("Something");
        Patient patient = new Patient("Mathangi", "Pushparajah", "12345");
        department.addPatient(patient);

        try{
            Patient patient1 = new Patient("Luciana", "And", "23456");
            department.remove(patient1);
        } catch (Exception error){
            assertEquals("Object cannot be found: The patient- And, Luciana is not registered " +
                            "in the hospital", error.getMessage());
        }
    }
}
